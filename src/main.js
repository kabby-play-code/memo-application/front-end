import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import 'es6-promise/auto'


Vue.prototype.$http = axios
new Vue({
  el: '#app',
  render: h => h(App)
})
